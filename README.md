# README #

This addon will brings Kodi into your home automation !

You will be able to send orders to your home automation box for each event happening in Kodi like : play, pause, stop, or depending on the screensaver status.
Even better, those orders are different if this is a movie you are playing, a TV Show, or the genre of music you are playing !

For exemple, swith the lights off and start the videoprojector when a movie start, or turn on your leds and your disco spots while listening to Dance music !!!
Orders are sent either with an xPL message (a home automation protocol) or simply by calling an http link.
Kodi will also respond to media commands and osd messages !

##  What do you need ##

You need to have Kodi installed. For more info : http://kodi.tv/

If you want to be able to receive xPL orders to replicate them, you also need a xPL hub installed on the same computer. See http://xplproject.org.uk/  for more info.

## How to install ##

Simply [download the latest version available on this site](https://bitbucket.org/masterbox/kodi-home-automation/downloads), then from Kodi, go into *System*, *Extensions* or *Addon*, and finally *Install from zip*. Then choose the zip you downloaded.
This add will be found in the *service* category under the name **"Kodi Home Automation"**.

## Configuration ##

The configuration is splitted in 6 tabs.

### General ###
You will be able to setup delay between orders, but also activate orders for the screensaver on or off.

If you click on one of those options, you will be able to setup up to 3 xPL actions and 3 IP actions.

For each xPL action, if you activate it, you will have to define a class and some parameters. Each parameter has to be separated by a *&* (this will be replaced by a line separator), please refer to the object or the module you want to control to know what parameter to use.
There is a special button called *Intercept the next xPL order*.
If you have a xPL hub installed on this computer, by clicking on this button, the class and parameter xill automatically contain those of the next xPL order seen on the network.
For exemple, you have your TV switched on by a Chacon RF remote, and you have a RFXCOM xPL on your network.
By simply pressing the *Intercept the next xPL order* and then the button of your remote used to switch on your TV, Kodi will be able to switch your TV on when the screensaver is deactivated.

For each IP action, there is only a URL to indicate. For example, if you have a KIRA 128 which is used to switch on your HIFI, you can put here the right URL to send the IR code to switch it on, and then, getting out of the screensaver will also switch on your HIFI !

### Movies ###

You can set orders for starting, pausing, and ending a movie.
The orders setup is the same as for sreensavers.
For exemple in my setup, when a movie start :
- The TV is turned off
- The videoprojector is turned on
- the light is switched off
- If I had a motored screen, it would be activated.

Then, if I press pause during a movie, the light is dimmed up.

Finally, when the movie is stopped :
- The TV is turned on
- The videoprojector is turned off
- the light is switched on
- If I had a motored screen, it would be removed.

### TV Shows ###
It's the same, but for TV shows.

### Music all genre ###
It allows the same things as movies and TV shows, with one more parameter defining the volume. Each time music is played, this volume will be set. If needed, I will make this optional.

###Music genre 1, 2 and 3 ###
* The 3 last tabs are also used for music but this time, actions are depending on the genre of the music played. Thus you need to indicate the genre you want. 
You can configure one tab to refer to multiple genres, simply separate them by a *|*. 
You need to know that it's not an exact match, if you only put a word, all genre containing this word will activate those orders.